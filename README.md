# Projet SY15 Turtlebot

Ceci est le dépôt de travail de Pierre Mirambeau et Robin Bouvier dans le cadre du projet de conduite autonome de robots Turtlebot3 de l'UV SY15.

## Problématique

Notre but est de faire évoluer le Turtlebot autour d'un îlot central. Lorsque celui-ci rencontre sa cible, il doit en signaler la position à l'opérateur. Puis, il doit retourner à son point de départ en prenant soin de ne pas entrer en collision avec des obstacles qui auront été placés entre temps. 

## Idées

Le robot suivrait un certain nombre de règles simples :
1. Ne pas entrer en collision avec des obstacles. Dans la pratique, cela signifie que si le Lidar détecte un objet à moins de 10 cm, le robot ne peut continuer à se déplacer dans la direction de celui-ci.
2. Essayer de réduire la distance le séparant de sa cible, ou au moins limiter au maximum l'augmentation de cette distance.


Cela signifie qu'à chaque évaluation le robot observe les points qu'il peut atteindre et détermine lequel est le plus optimal pour répondre à ces règles, puis il s'y rend.
