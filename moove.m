
sub_odom = rossubscriber('/odom', "nav_msgs/Odometry");
sub_lidar = rossubscriber('/scan', "sensor_msgs/LaserScan");
sub_cam = rossubscriber('/turtlebotcam/image_raw/compressed');

moove(0.1, 0.01, 0.001, 0.2)

% velocity_msg.Linear.X = 0.0;
% velocity_msg.Angular.Z = 0;
% pub_vel.send(velocity_msg);


function moove(distance, vitesse_lin, vitesse_ang, angle)
    pub_vel = rospublisher("/cmd_vel", "geometry_msgs/Twist");
    velocity_msg = rosmessage("geometry_msgs/Twist")
    Temps_rotation = angle/vitesse_ang
    Temps_lineaire = distance/vitesse_lin
    
    velocity_msg.Angular.Z = vitesse_ang;
    pub_vel.send(velocity_msg);
    ros::Duration(Temps_rotation).sleep();
    
    velocity_msg.Linear.X = vitese_lin;
    pub_vel.send(velocity_msg);
    time.sleep(Temps_lineaire);
end
